<?php

function ouvrirConnexionMysql(){
    global $connexionMysql;
    if($connexionMysql==null){
    $connexionMysql = mysqli_connect('localhost','root','password','baseEntissab');
   mysqli_set_charset($connexionMysql,"utf8");

        }  
}

function fermerConnexionMysql(){
    global $connexionMysql;
    if($connexionMysql){
    mysqli_close($connexionMysql);
    $connexionMysql = null;
        }
}


function ouvrirConnexion(){
    global $conn;
    if($conn==null)
    $conn = oci_connect('login', 'password', 'host','WE8ISO8859P9');   
}

function fermerConnexion(){
    global $conn;
    if($conn){
    oci_close($conn);
	$conn=null;
	}
}

function ExisteDansApogee($cne){
    ouvrirConnexion();
    global $conn;
    if (!$conn) {
          $e = oci_error();
          trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    } 
    else{
        $sql = "SELECT * FROM individu WHERE cod_nne_ind like '$cne'";
        $stid = oci_parse($conn, $sql);
        oci_execute($stid);

    if (oci_fetch($stid)) {
       fermerConnexion();
       return true;
    }
    oci_free_statement($stid);
    fermerConnexion();
    return  false;
    }
}

function ExisteAnuDansAnu($Annee,$COD_IND){
    ouvrirConnexion();
    global $conn;
    if (!$conn) {
          $e = oci_error();
          trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    } 
    else{
        $sql = "SELECT * FROM ins_adm_anu WHERE cod_anu like '$Annee' and cod_ind like '$COD_IND'";
        $stid = oci_parse($conn, $sql);
        oci_execute($stid);

    if (oci_fetch($stid)) {
       fermerConnexion();
       return true;
    }
    oci_free_statement($stid);
    fermerConnexion();
    return  false;
    }
}


function ExisteEtpDansEtp($Annee,$COD_IND,$COD_ETP){
    ouvrirConnexion();
    global $conn;
    if (!$conn) {
          $e = oci_error();
          trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    } 
    else{
        $sql = "SELECT * FROM ins_adm_etp WHERE cod_anu like '$Annee' and cod_ind like '$COD_IND' and cod_etp like '$COD_ETP' ";
        $stid = oci_parse($conn, $sql);
        oci_execute($stid);

    if (oci_fetch($stid)) {
       fermerConnexion();
       return true;
    }
    oci_free_statement($stid);
    fermerConnexion();
    return  false;
    }
}


function ramenerMoiLesSixChampsDuDerinerAnnee($cne){
    ouvrirConnexion();
    global $conn;
    if (!$conn) {
          $e = oci_error();
          trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    } 
    else{
        $sql = "select cod_ind, COD_TPE_ANT,COD_ETB_ANT,COD_DEP_ANT,COD_SIS_ANN_PRE,COD_DEP_ANN_PRE,COD_TDS_ANN_PRE from (select *  from ins_adm_anu order  by cod_anu  desc) where cod_ind  = (select cod_ind from individu where  cod_nne_ind like '$cne') and rownum=1";
        $stid = oci_parse($conn, $sql);
        oci_execute($stid);
        $nrows = oci_fetch_all($stid, $res);
        oci_free_statement($stid);
        fermerConnexion();
        return $res;  
    }
}
    
function  inscrire_apogee($idFiliere,$Annee){ 
    ouvrirConnexion();
    global $conn;
    if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    
    else{   
            $cne = $_SESSION['COD_NNE_IND'];
        
   //Extraction des  informations d'étapes et diplomes 
        
        ouvrirConnexionMysql();
        global $connexionMysql;
     $reponse = mysqli_query($connexionMysql,"CALL getEtapAndDip ('$idFiliere')");
     if($reponse){
         $donnees = mysqli_fetch_array($reponse,MYSQLI_BOTH);
     $COD_ETP = $donnees['cod_etp'];
     $COD_VRS_VET = $donnees['cod_vrs_vet'];
     $COD_DIP =  $donnees['cod_dip'];
     $COD_VRS_VDI = $donnees['cod_vrs_vdi'];
     $COD_CGE = $donnees['cod_cge'];
     $COD_CMP = $donnees['cod_cmp'];
     $utilisateur = $_SESSION['utilisateur'];
     }          
     fermerConnexionMysql();
        
    ///////////////////////////////////////////////// 
    $AnneeAnt = $Annee -1;
    $uti = strtoupper($utilisateur);
    ////////////////////////////////////////////////
    echo '<br>'.strtoupper($uti).'</br>';
    if (ExisteDansApogee($cne)){

        $mesSixChamps = ramenerMoiLesSixChampsDuDerinerAnnee($cne);
        $COD_IND = $mesSixChamps['COD_IND'][0];
        $COD_TPE_ANT = $mesSixChamps['COD_TPE_ANT'][0];
        $COD_ETB_ANT = $mesSixChamps['COD_ETB_ANT'][0];
        $COD_DEP_ANT = $mesSixChamps['COD_DEP_ANT'][0];
        $COD_SIS_ANN_PRE = $mesSixChamps['COD_SIS_ANN_PRE'][0];
        $COD_DEP_ANN_PRE = $mesSixChamps['COD_DEP_ANN_PRE'][0];
        $COD_TDS_ANN_PRE = $mesSixChamps['COD_TDS_ANN_PRE'][0];
        
        /* Début INS_INFO_ANU */  
             	$insert_INS_INFO_ANU = "Insert into INS_INFO_ANU
        (COD_ANU, COD_IND, COD_THB)
 Values (
		'$Annee'
        ,'".$COD_IND."'
        , '4')"; 
     if(!ExisteAnuDansAnu($Annee,$COD_IND)){
        ouvrirConnexion();
        $stmt_INS_INFO_ANU = oci_parse($conn, $insert_INS_INFO_ANU);
		oci_execute($stmt_INS_INFO_ANU,OCI_NO_AUTO_COMMIT);
        }
        /* Fin INS_INFO_ANU */  
        
       /* Début INS_ADM_ANU */   
       	$insert_INS_ADM_ANU = "Insert into INS_ADM_ANU
   (COD_ANU, COD_IND, COD_PRU,  
    COD_RGI,  
    COD_SOC, COD_STU, 
    COD_TPE_ANT, COD_ETB_ANT, COD_DEP_ANT, 
    COD_ETR, COD_PCS_ETUDIANT, COD_PCS_PARENT, COD_UTI,  
    TEM_AFL_SSO, TEM_TRM_SSO_IAA, 
    DAA_ETB_ANT_IAA,
   DAT_CRE_IAA,
    ETA_IAA, 
    COD_SIS_ANN_PRE, COD_ETB_ANN_PRE, COD_DEP_ANN_PRE, COD_TDS_ANN_PRE, COD_PCS_MERE)
 Values
   ('$Annee', $COD_IND, 'NO',  
    '1', 
    'NO', '01',  
    '".$COD_TPE_ANT."', '$COD_ETB_ANT', '".$COD_DEP_ANT."',
    NULL, '82', '".$_SESSION['COD_PCS_PARENT']."', '$uti',  
    'N', 'N', 
    '$AnneeAnt', 
    sysdate,
    'E',
    '$COD_SIS_ANN_PRE', '$COD_ETB_ANT', '".$COD_DEP_ANN_PRE."', '$COD_TDS_ANN_PRE', '".$_SESSION['COD_PCS_MERE']."')"; 

        if(!ExisteAnuDansAnu($Annee,$COD_IND)){
        ouvrirConnexion();
        $stmt_INS_ADM_ANU = oci_parse($conn, $insert_INS_ADM_ANU);
		oci_execute($stmt_INS_ADM_ANU,OCI_NO_AUTO_COMMIT);  
        }                      
        /* Fin INS_ADM_ANU */

         $NBR_INS_ETP = "(select count(*)+1 from ins_adm_etp where cod_ind like '".$COD_IND."' and cod_etp like '".$COD_ETP."')";  
         $NBR_INS_DIP = "(select count(*)+1 from ins_adm_etp where cod_ind like '".$COD_IND."' and COD_DIP like '".$COD_DIP."')";  
         $NBR_INS_CYC = "(select max(NBR_INS_CYC)+1 from ins_adm_etp where cod_ind like '".$COD_IND."')";  
		 
        /* Début INS_ADM_ETP */     
       	$insert_INS_ADM_ETP = "Insert into INS_ADM_ETP
   (COD_ANU, COD_IND, COD_ETP, COD_VRS_VET, NUM_OCC_IAE, 
    COD_DIP, COD_VRS_VDI, COD_CGE, COD_CMP, 
    COD_UTI, DAT_CRE_IAE, 
    ETA_IAE, ETA_PMT_IAE, NBR_INS_CYC, NBR_INS_ETP, 
    TEM_TLS_IAE, TEM_IAE_PRM, TEM_CHG_SUR_ETP, 
    TEM_INS_ELP_ATMQ_IAE, COD_NAT_TIT_ACC_IAE, 
    NBR_INS_DIP,
    COD_PRU)
 Values
   ('$Annee', $COD_IND, '$COD_ETP', $COD_VRS_VET, 1, 
    '$COD_DIP', $COD_VRS_VDI, '$COD_CGE', '$COD_CMP', 
    '$uti', sysdate, 
    'E', 'P', $NBR_INS_CYC, $NBR_INS_ETP, 
    'N', 'O', 'N', 
    'O', 'A',  
     $NBR_INS_DIP, 
    'NO')"; 
	if(!ExisteEtpDansEtp($Annee,$COD_IND,$COD_ETP)){
        ouvrirConnexion();
        $stmt_INS_ADM_ETP = oci_parse($conn, $insert_INS_ADM_ETP);
		oci_execute($stmt_INS_ADM_ETP,OCI_NO_AUTO_COMMIT); 
	}		
        /* Fin INS_ADM_ETP */
        if($conn != null){
        oci_commit($conn);
        fermerConnexion();
        }
    }
        
    else {
    ouvrirConnexion();		
    $req_sequences = 'SELECT seq_cod_ind.nextval as "cod_ind",seq_cod_adr.nextval as "cod_adr",seq_cod_etu.nextval as "cod_etu" FROM dual';
    $stid = oci_parse($conn, $req_sequences);
    oci_execute($stid);
    oci_fetch($stid);
    
    $COD_IND = oci_result($stid, 'cod_ind');
    $COD_ETU = oci_result($stid, 'cod_etu');
    $COD_ADR = oci_result($stid, 'cod_adr');  
    oci_free_statement($stid);
    
        
    //$COD_IND = 300000;
   // $COD_ETU = 9100000;
  //  $COD_ADR = 900000;

    $req_sequences = 'SELECT seq_cod_adr.nextval as "cod_adr" FROM dual';
    $stid = oci_parse($conn, $req_sequences);
    oci_execute($stid);
    oci_fetch($stid);
    $COD_ADR2 = oci_result($stid, 'cod_adr');  
    oci_free_statement($stid);
  
        
    $Annee_lib = iconv('utf-8','windows-1256//TRANSLIT',"Année universitaire 2021_2022" );
    
    $DATE_NAI_IND = $_SESSION['DATE_NAI_IND'];
    
    
 $LIB_NOM_PATAR_IND_OPI = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['LIB_NOM_IND_ARB'] ));
 $LIB_PRAR_IND_OPI = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['LIB_PRN_IND_ARB'] ));
 $LIB_VILAR_NAI_ETU_OPI = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['LIB_VIL_NAI_ETU_ARB'] ));

  $LIB_NOM_PAT_IND_OPI = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['LIB_NOM_PAT_IND'] ));
 $LIB_PR1_IND_OPI = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['LIB_PR1_IND'] ));
 $LIB_VIL_NAI_ETU_OPI = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['LIB_VIL_NAI_ETU'] ));
    
 $adresse1 = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['adresse1'] ));
 $adresse2 = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['adresse2'] ));
 $email = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['email'] ));
 $tel = str_replace("'","''",iconv('utf-8','windows-1256//TRANSLIT',$_SESSION['tel'] ));
    
    /* Début INDIVIDU */
    
	$insert_INDIVIDU = "INSERT INTO INDIVIDU
   (COD_IND, COD_FAM, COD_SIM, COD_PAY_NAT, COD_ETB, 
    COD_UTI, COD_UTI_MOD, COD_NNE_IND, DAT_CRE_IND, DAT_MOD_IND, 
    DATE_NAI_IND, TEM_DATE_NAI_REL, DAA_LBT_IND, DMM_LBT_IND, DAA_ENT_ETB, 
    LIB_NOM_PAT_IND, LIB_PR1_IND, ETA_PRS_ETU, COD_ETU, COD_CIV, 
    TEM_CRT_SSO_ETU, COD_SEX_ETU, LIB_VIL_NAI_ETU, ETA_COH_DOS_IND, COD_DEP_PAY_NAI, 
    COD_TYP_DEP_PAY_NAI, TEM_FAU_IND, TEM_AID_IND, TEM_TIER_IND, COD_ANU_SRT_IND, 
    TEM_SAN_OBJ_MER_IND, TEM_SAN_OBJ_PER_IND, TEM_NNE_PRV_IND, DAA_ENS_SUP, DAA_ETB, 
    TEM_RGL_SIT_MIL, LIB_NOM_IND_ARB, LIB_PRN_IND_ARB, CIN_IND, LIB_VIL_NAI_ETU_ARB)
 Values (
		$COD_IND
        ,'".$_SESSION['COD_FAM']."'
        , '4'
        , '".$_SESSION['COD_PAY_NAT']."'
        , '19000009' 
        ,'$uti'
        , ''
        , '".$cne."'
        , sysdate 
        , ''
        , TO_DATE('$DATE_NAI_IND', 'DD/MM/YYYY')
        , 'N'
        , '0'
        , '0'
        , '".$_SESSION['DAA_ENT_ETB']."' 
        , '".$LIB_NOM_PAT_IND_OPI."'
        , '".$LIB_PR1_IND_OPI."'
        , 'O'
        , $COD_ETU
        , '1'
        , 'N'
        , '".$_SESSION['COD_SEX_OPI']."'
        , '".$LIB_VIL_NAI_ETU_OPI."'
        , 'VAIA'
        , '".$_SESSION['COD_DEP_PAY_NAI']."'
        ,  'D'
        , 'N'
        , 'N'
        , 'N'
        , ''
        , 'N'
        , 'N'
        , 'N'
        , '".$_SESSION['DAA_ENS_SUP']."'
        , '".$_SESSION['DAA_ENT_ETB']."'      
        , 'O'
        , '".$LIB_NOM_PATAR_IND_OPI."'
        , '".$LIB_PRAR_IND_OPI."'
        , '".$_SESSION['CIN_IND']."'
        , '".$LIB_VILAR_NAI_ETU_OPI."')";  
    
    	$stmt_INDIVIDU_oci = oci_parse($conn, $insert_INDIVIDU);
		oci_execute($stmt_INDIVIDU_oci,OCI_NO_AUTO_COMMIT);
    
    /* Fin INDIVIDU */
    
    /* Début INS_INFO_ANU */

    
     	$insert_INS_INFO_ANU = "Insert into INS_INFO_ANU
        (COD_ANU, COD_IND, COD_THB)
 Values (
		'$Annee'
        ,'".$COD_IND."'
        , '4')"; 
    
        $stmt_INS_INFO_ANU = oci_parse($conn, $insert_INS_INFO_ANU);
		oci_execute($stmt_INS_INFO_ANU,OCI_NO_AUTO_COMMIT);
        /* Fin INS_INFO_ANU */    
 
        /* Début adresse */    
     	$insert_adresse = "INSERT ALL
 INTO ADRESSE
   (COD_ADR, COD_IND, COD_ANU_INA, COD_IND_INA, COD_PAY, 
    LIB_AD1, LIB_AD2, LIB_AD3, 
     NUM_TEL, COD_DEP)
 Values
   ($COD_ADR, $COD_IND, NULL, NULL, '".$_SESSION['COD_PAY_NAT']."', 
    '".$adresse1."', '".$adresse2."', '".$email."',
     '".$tel."', '".$_SESSION['COD_DEP_BAC']."')
 INTO ADRESSE (COD_ADR, COD_IND, COD_ANU_INA, COD_IND_INA, COD_PAY, 
    LIB_AD1, LIB_AD2, LIB_AD3, 
     NUM_TEL, COD_DEP)
 Values
	($COD_ADR2,'' ,$Annee, $COD_IND, '".$_SESSION['COD_PAY_NAT']."', 
    '".$adresse1."', '".$adresse2."', '".$email."',
     '".$tel."', '".$_SESSION['COD_DEP_BAC']."')
 select * from dual"; 

        $stmt_adresse = oci_parse($conn, $insert_adresse);
		oci_execute($stmt_adresse,OCI_NO_AUTO_COMMIT);
	    /* Fin adresse */
    
        /* Début IND_BAC */ 
        $cod_etb = "(select cod_etb from etablissement where cod_tpe like '".$_SESSION['COD_TPE']."' and cod_dep like '".$_SESSION['COD_DEP_BAC']."')";
       	$insert_IND_BAC = "Insert into IND_BAC
   (COD_IND, COD_BAC, COD_ETB, COD_TPE, COD_DEP, 
    COD_MNB, DAA_OBT_BAC_IBA, TEM_INS_ADM)
 Values
   ($COD_IND, '".$_SESSION['COD_BAC']."', $cod_etb, '".$_SESSION['COD_TPE']."', '".$_SESSION['COD_DEP_BAC']."', 
    '".$_SESSION['COD_MNB']."', '".$_SESSION['anneeobtbac']."', 'O')"; 

        $stmt_IND_BAC = oci_parse($conn, $insert_IND_BAC);
		oci_execute($stmt_IND_BAC,OCI_NO_AUTO_COMMIT);       
  	    /* Fin IND_BAC */
    
        /* Début INS_ADM_ANU */   
       	$insert_INS_ADM_ANU = "Insert into INS_ADM_ANU
   (COD_ANU, COD_IND, COD_PRU,  
    COD_RGI,  
    COD_SOC, COD_STU, 
    COD_TPE_ANT, COD_ETB_ANT, COD_DEP_ANT, 
    COD_ETR, COD_PCS_ETUDIANT, COD_PCS_PARENT, COD_UTI,  
    TEM_AFL_SSO, TEM_TRM_SSO_IAA, 
    DAA_ETB_ANT_IAA,
   DAT_CRE_IAA,
    ETA_IAA, 
    COD_SIS_ANN_PRE, COD_ETB_ANN_PRE, COD_DEP_ANN_PRE, COD_TDS_ANN_PRE, COD_PCS_MERE)
 Values
   ('$Annee', $COD_IND, 'NO',  
    '1', 
    'NO', '01',  
    '".$_SESSION['COD_TPE']."', $cod_etb, '".$_SESSION['COD_DEP_BAC']."',
    NULL, '82', '".$_SESSION['COD_PCS_PARENT']."', '$uti',  
    'N', 'N', 
    '$AnneeAnt', 
    sysdate,
    'E',
    'K', $cod_etb, '".$_SESSION['COD_DEP_BAC']."', 'A', '".$_SESSION['COD_PCS_MERE']."')"; 

        $stmt_INS_ADM_ANU = oci_parse($conn, $insert_INS_ADM_ANU);
		oci_execute($stmt_INS_ADM_ANU,OCI_NO_AUTO_COMMIT);                       
        /* Fin INS_ADM_ANU */
    
        /* Début INS_ADM_ETP */     
       	$insert_INS_ADM_ETP = "Insert into INS_ADM_ETP
   (COD_ANU, COD_IND, COD_ETP, COD_VRS_VET, NUM_OCC_IAE, 
    COD_DIP, COD_VRS_VDI, COD_CGE, COD_CMP, 
    COD_UTI, DAT_CRE_IAE, 
    ETA_IAE, ETA_PMT_IAE, NBR_INS_CYC, NBR_INS_ETP, 
    TEM_TLS_IAE, TEM_IAE_PRM, TEM_CHG_SUR_ETP, 
    TEM_INS_ELP_ATMQ_IAE, COD_NAT_TIT_ACC_IAE, 
    NBR_INS_DIP,
    COD_PRU)
 Values
   ('$Annee', $COD_IND, '$COD_ETP', $COD_VRS_VET, 1, 
    '$COD_DIP', $COD_VRS_VDI, '$COD_CGE', '$COD_CMP',
    '$uti', sysdate, 
    'E', 'P', 1, 1, 
    'N', 'O', 'N', 
    'O', 'A',  
     1, 
    'NO')"; 

        $stmt_INS_ADM_ETP = oci_parse($conn, $insert_INS_ADM_ETP);
		oci_execute($stmt_INS_ADM_ETP,OCI_NO_AUTO_COMMIT);      
        /* Fin INS_ADM_ETP */
		
		oci_commit($conn);
        fermerConnexion();
		

    }

 }
    //supprimerCetEtudiantDansLesAutresFilières($idFiliere, $cne);
	marqueCommeInscrit($idFiliere, $cne);
}
		
function marqueCommeInscrit($filiere, $cne){
    ouvrirConnexionMysql();
    global $connexionMysql;
    $reponse = mysqli_query($connexionMysql,"CALL marqueCommeInscrit ('$cne',$filiere)");
    
    fermerConnexionMysql();
}

function ramenerMoiLesInfoDeCetEtudiant($cne){
    
        ouvrirConnexionMysql();
    global $connexionMysql;
    $ETB = $_SESSION['id_etab'];
    $reponse = mysqli_query($connexionMysql,"CALL getDetailsEtudiantByCne ('$cne',$ETB)");
     if($reponse){
                    $donnees = mysqli_fetch_array($reponse,MYSQLI_BOTH);
    
    $_GET['id_fil'] = $donnees['id_fil'];
    $_GET['image'] = $donnees['imageName'];
 $_SESSION['existeImage'] = $donnees['imageExiste'];  

    $_GET['LIB_PR1_IND_OPI'] = $donnees['prenom'];
    $_GET['LIB_PRAR_IND_OPI'] = $donnees['prenomar'];
    $_GET['LIB_NOM_PAT_IND_OPI'] = $donnees['nom'];
    $_GET['LIB_NOM_PATAR_IND_OPI'] = $donnees['nomar'];
    $_GET['CIN_IND_OPI']  = $donnees['cin'];
    $_GET['LIB_VIL_NAI_ETU_OPI'] = $donnees['villedtn']; 
    $_GET['LIB_VILAR_NAI_ETU_OPI'] = $donnees['villedtnar'];
    $_GET['LIB_AD1'] = $donnees['adresse1'];
    $_GET['LIB_AD2'] = $donnees['adresse2'];
    $_GET['LIB_AD3'] = $donnees['email'];
    $_GET['NUM_TEL'] = $donnees['tel'];
        $_GET['COD_DEP_PAY_NAI'] = $donnees['provincedtn']; //**
        $_GET['COD_SEX_OPI'] = $donnees['sexe']; //**()
        $_GET['COD_PAY_NAT'] = $donnees['nationalite']; //**
        $_GET['COD_FAM'] = $donnees['situationfam']; //**
        $_GET['DAA_OBT_BAC_OBA'] = $donnees['anneeobtbac']; //**
        $_GET['COD_BAC'] = $donnees['seriebac']; //**
        $_GET['COD_MNB'] = $donnees['mentionbac']; //**
        $_GET['COD_TPE'] = $donnees['typeetab']; //**
        $_GET['COD_DEP'] = $donnees['provinceetab']; //**
        $_GET['DAA_ENS_SUP_OPI'] = $donnees['enssup']; //**
        $_GET['DAA_ENT_ETB_OPI'] = $donnees['ump']; //**
        $_GET['DAA_ETB_OPI'] = $donnees['ump']; //**
        $_GET['COD_DEP_ADRESSE_OPI'] = $donnees['provincebac'];      
        $_GET['COD_PAY_ADRESSE_OPI'] = $donnees['paysbac']; //**      
        $_GET['COD_LIB_CAT_P'] = $donnees['profpere']; //**
        $_GET['COD_LIB_CAT_M'] = $donnees['profmere']; //**   
     }
   
   fermerConnexionMysql();
        
   //Extraire les informations de l'AMO 
        ouvrirConnexionMysql();
    global $connexionMysql;
     $reponse = mysqli_query($connexionMysql,"select * from amo_opi where cod_nne_ind like '$cne' ORDER BY ID_IND desc LIMIT 1");
     if($reponse){
                    $donnees = mysqli_fetch_array($reponse,MYSQLI_BOTH);
    $_GET['CODE_POSTAL_ETU_OPI'] =  $donnees['COD_POST'];
    //$_GET['TEM_MARIE'] = $donnees['TEM_MARIE'];
    //$_GET['TEM_DIVORCE'] = $donnees['TEM_DIVORCE']; 
   // $_GET['TEM_VEUF'] = $donnees['TEM_VEUF'];

        if($_GET['COD_FAM'] == "CELIBATAIRE"){  
         $_GET['compteCelib'] = $donnees['COD_RIB'];
              $_GET['nomP'] = $donnees['NOM_PERE'];
            $_GET['prenomP'] = $donnees['PRENOM_PERE'];
                    $_GET['datenaiP'] = $donnees['DATE_NAIS_PERE'];
              $_GET['decededatep'] = $donnees['DATE_DC_PERE'];
                $_GET['nomM'] = $donnees['NOM_MERE'];
            $_GET['prenomM'] = $donnees['PRENOM_MERE'];
             $_GET['datenaiM'] = $donnees['DATE_NAIS_MERE'];
             $_GET['decededatem'] = $donnees['DATE_DC_MERE'];
            $_GET['couverture'] = $donnees['NOM_COUVERTURE_MALADIE'];
             $_GET['ncniepar'] = $donnees['CNI_PERE'];
            $_GET['ncniemer'] = $donnees['CNI_MERE'];
            
            if($donnees['TEM_RIB_ETU']=='O')
            $_GET['TEM_RIB_ETUOC'] = "checked";
            else
            $_GET['TEM_RIB_ETUNC'] = "checked"; 

            if($donnees['TEM_CNI_PERE']=='O')
            $_GET['TEM_CNI_PEREOC'] = "checked";
            else
            $_GET['TEM_CNI_PERENC'] = "checked";
            
             if($donnees['T_DC_PERE']=='O')
            $_GET['T_DC_PEREOC'] = "checked";
            else
            $_GET['T_DC_PERENC'] = "checked";
            
               if($donnees['TEM_CNI_MERE']=='O')
            $_GET['TEM_CNI_MEREOC'] = "checked";
            else
            $_GET['TEM_CNI_MERENC'] = "checked";

              if($donnees['TEM_DC_MERE']=='O')
            $_GET['TEM_DC_MEREOC'] = "checked";
            else
            $_GET['TEM_DC_MERENC'] = "checked";
            
           if($donnees['TEM_COUVERTURE_MALADIE']=='O')
            $_GET['TEM_COUVERTURE_MALADIEOC'] = "checked";
            else
            $_GET['TEM_COUVERTURE_MALADIENC'] = "checked";
            
     
 
     
   }
        if($_GET['COD_FAM'] == "MARIE"){  
         $_GET['compteMarie'] = $donnees['COD_RIB'];
            $_GET['couverturem'] = $donnees['NOM_COUVERTURE_MALADIE'];
              $_GET['ncnieconj'] = $donnees['CNI_CONJOINT'];
            
            if($donnees['TEM_RIB_ETU']=='O')
            $_GET['TEM_RIB_ETUOM'] = "checked";
            else
            $_GET['TEM_RIB_ETUNM'] = "checked"; 
            
                       if($donnees['TEM_COUVERTURE_MALADIE']=='O')
            $_GET['TEM_COUVERTURE_MALADIEOM'] = "checked";
            else
            $_GET['TEM_COUVERTURE_MALADIENM'] = "checked";         
   }
        if($_GET['COD_FAM'] == "DIVORCE(E)"){  
         $_GET['compteDivo'] = $donnees['COD_RIB'];
              $_GET['nomPD'] = $donnees['NOM_PERE'];
            $_GET['prenomPD'] = $donnees['PRENOM_PERE'];
                    $_GET['datenaiPD'] = $donnees['DATE_NAIS_PERE'];
             $_GET['decededatepd'] = $donnees['DATE_DC_PERE'];
                $_GET['nomMD'] = $donnees['NOM_MERE'];
            $_GET['prenomMD'] = $donnees['PRENOM_MERE'];
             $_GET['datenaiMD'] = $donnees['DATE_NAIS_MERE'];
             $_GET['decededatemd'] = $donnees['DATE_DC_MERE'];
            $_GET['couvertured'] = $donnees['NOM_COUVERTURE_MALADIE'];
            $_GET['datediv'] = $donnees['DATE_DIVORCE'];
             $_GET['ncniepd'] = $donnees['CNI_PERE'];
            $_GET['ncniemd'] = $donnees['CNI_MERE'];
              $_GET['ncniecd'] = $donnees['CNI_CONJOINT'];
            
            if($donnees['TEM_RIB_ETU']=='O')
            $_GET['TEM_RIB_ETUOD'] = "checked";
            else
            $_GET['TEM_RIB_ETUND'] = "checked"; 
            
            if($donnees['TEM_CNI_PERE']=='O')
            $_GET['TEM_CNI_PEREOD'] = "checked";
            else
            $_GET['TEM_CNI_PEREND'] = "checked";
            
                         if($donnees['T_DC_PERE']=='O')
            $_GET['T_DC_PEREOD'] = "checked";
            else
            $_GET['T_DC_PEREND'] = "checked";
            
                           if($donnees['TEM_CNI_MERE']=='O')
            $_GET['TEM_CNI_MEREOD'] = "checked";
            else
            $_GET['TEM_CNI_MEREND'] = "checked";
            
                          if($donnees['TEM_DC_MERE']=='O')
            $_GET['TEM_DC_MEREOD'] = "checked";
            else
            $_GET['TEM_DC_MEREND'] = "checked";
            
                       if($donnees['TEM_COUVERTURE_MALADIE']=='O')
            $_GET['TEM_COUVERTURE_MALADIEOD'] = "checked";
            else
            $_GET['TEM_COUVERTURE_MALADIEND'] = "checked";
            
            
   }
        if($_GET['COD_FAM'] == "VEUF(VE)"){  
         $_GET['compteVeuf'] = $donnees['COD_RIB'];
              $_GET['nomPV'] = $donnees['NOM_PERE'];
            $_GET['prenomPV'] = $donnees['PRENOM_PERE'];
                    $_GET['datenaiPV'] = $donnees['DATE_NAIS_PERE'];
             $_GET['decededatepv'] = $donnees['DATE_DC_PERE'];
                $_GET['nomMV'] = $donnees['NOM_MERE'];
            $_GET['prenomMV'] = $donnees['PRENOM_MERE'];
             $_GET['datenaiMV'] = $donnees['DATE_NAIS_MERE'];
             $_GET['decededatemv'] = $donnees['DATE_DC_MERE'];
            $_GET['couverturev'] = $donnees['NOM_COUVERTURE_MALADIE'];
             $_GET['datev'] = $donnees['DATE_DC_CONJOINT'];
             $_GET['ncniepv'] = $donnees['CNI_PERE'];
            $_GET['ncniemv'] = $donnees['CNI_MERE'];
              $_GET['ncniecv'] = $donnees['CNI_CONJOINT'];
            
            if($donnees['TEM_RIB_ETU']=='O')
            $_GET['TEM_RIB_ETUOV'] = "checked";
            else
            $_GET['TEM_RIB_ETUNV'] = "checked";   
            
            if($donnees['TEM_CNI_PERE']=='O')
            $_GET['TEM_CNI_PEREOV'] = "checked";
            else
            $_GET['TEM_CNI_PERENV'] = "checked";
            
                         if($donnees['T_DC_PERE']=='O')
            $_GET['T_DC_PEREOV'] = "checked";
            else
            $_GET['T_DC_PERENV'] = "checked";
            
                           if($donnees['TEM_CNI_MERE']=='O')
            $_GET['TEM_CNI_MEREOV'] = "checked";
            else
            $_GET['TTEM_CNI_MERENV'] = "checked";
            
                          if($donnees['TEM_DC_MERE']=='O')
            $_GET['TEM_DC_MEREOV'] = "checked";
            else
            $_GET['TEM_DC_MERENV'] = "checked";
            
                       if($donnees['TEM_COUVERTURE_MALADIE']=='O')
            $_GET['TEM_COUVERTURE_MALADIEOV'] = "checked";
            else
            $_GET['TEM_COUVERTURE_MALADIENV'] = "checked";
   }
    }
    fermerConnexionMysql();
    
}

function supprimerCetEtudiant($cne){
    ouvrirConnexionMysql();
    global $connexionMysql;
    $reponse = mysqli_query($connexionMysql,"CALL supprimerCetEtudiant ('$cne')");
    
    fermerConnexionMysql();
}


function supprimerCetEtudiantDansLesAutresFilières($filiere, $cne){
    ouvrirConnexionMysql();
    global $connexionMysql;
    $reponse = mysqli_query($connexionMysql,"CALL supprimerCetEtudiantDansLesAutresFilières ('$cne',$filiere)");
    
    fermerConnexionMysql();
}


function DejaExisteCne($cne){
    ouvrirConnexionMysql();
    global $connexionMysql;
    $reponse = mysqli_query($connexionMysql," SELECT `ID_FIL`,`statut` FROM `choix` c inner join ind_opi i on i.id_ind = c.id_ind where cod_nne_ind_opi like '$cne'");
     if($donnees = mysqli_fetch_array($reponse,MYSQLI_BOTH)){
          if ($donnees['ID_FIL'] != null){
              $_SESSION['ID_FILByCne'] = $donnees['ID_FIL'];
              $_SESSION['idStatutByCne'] = $donnees['statut'];   
              return true;
          }
  
    else{
        return false;
    }       
     } 
    fermerConnexionMysql();
      }
 
        ouvrirConnexionMysql();
     global $connexionMysql;
        $reponse = mysqli_query($connexionMysql,"call insertionCon($id_login,'$ip','$user_agent','$date_con','$desc')");
        
        fermerConnexionMysql();
       
}

?>